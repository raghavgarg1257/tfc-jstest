#!/usr/bin/env bash

echo 'Moving to new branch [js-implementation]'
git checkout js-implementation

echo 'Installing dependencies'
npm install

echo 'Building client files'
npm run build

echo 'Starting the server'
npm run serve:dev
